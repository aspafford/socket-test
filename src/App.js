import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
} from 'react-router-dom';
import React from "react";
import ReactDOM from "react-dom";
import { applyMiddleware, compose, createStore } from 'redux'
import { Provider } from 'react-redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
// reducers
import reducer from './reducers/index.js'
// components
import { Main } from './components/Main.js'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(thunk, logger)
));

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route component={Main} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;

ReactDOM.render(<App />, document.getElementById("app"));

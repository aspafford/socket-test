import openSocket from 'socket.io-client';

const socket = openSocket('http://localhost:8080');

export const sensorData = payload => ({
  type: 'SENSOR_DATA',
  payload: payload
})

export const subscribeToTimer = (cb) => {
  return (dispatch) => {
    socket.on('timer', data => {
      dispatch(sensorData(data))
    });
    socket.emit('subscribeToTimer', 5000);
  }
}

import React, { Component } from 'react'
import { connect } from 'react-redux'

import { increment } from '../actions/index.js'

// actions

// components

const mapDispatchProperties = 
  dispatch => ({
    increment: () => {
      dispatch(increment());
    }
  })

const mapStateToProps = state => ({
})

const View = props =>
  <div onClick={props.increment}>Increment</div>

export const Increment = connect(mapStateToProps, mapDispatchProperties)(View)

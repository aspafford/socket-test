import React, { Component } from 'react'
import { connect } from 'react-redux'
// actions
import { subscribeToTimer } from '../actions/index.js'

const mapDispatchToProps = dispatch => {
  return {
    init: params => {
      dispatch(subscribeToTimer( (err, data) => console.log('data', data)))
    }
  }
}

const mapStateToProps = state => ({
  sensorData: state.sensorData
})

const Widget = props => {

  let {device, name, value, date} = props.props
  let d = new Date(date)
  
  return (
    <div>
      <div>Device: {device}</div>
      <div>Name: {name}</div>
      <div>Value: {value}</div>
      <div>Date: {d.toString()}</div>
    </div>
  )
}

class View extends Component {

  componentWillMount() {
    this.props.init()
  }

  render() {

    if (typeof this.props.sensorData === 'undefined') return <div/>

    return (
      <div className="container">
        <Widget props={this.props.sensorData}/>
      </div>
    )
  }
}

export const Main = connect(mapStateToProps, mapDispatchToProps)(View)
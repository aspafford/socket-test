const INITIAL_STATE = {}

export const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "INITIALIZE": {
      return {...state}
    }
    case "SET_COUNT": {

      console.log(action)
      
      return {...state, count: action.payload.data.count}
    }
  }
  return state
}

export default reducer
